$("#task").ready(() => {
    $("#task").submit(function (e) {
        e.preventDefault();

        validateForm();

        let action_url = e.currentTarget.action;

        $.ajax({
            url: action_url,
            type: 'post',
            data: $("#task").serialize(),
            success: function (data) {
                console.log(data);
                document.body.innerHTML = data;
            },
            failure: function (data) {
                console.log(data);
            }
        });

    })
});

$("#login").ready(() => {
    $("#login").submit(function (e) {
        e.preventDefault();
        console.log('login')

        validateForm();

        let action_url = e.currentTarget.action;

        $.ajax({
            url: action_url,
            type: 'post',
            data: $("#login").serialize(),
            success: function (data) {
                document.body.innerHTML = data;
            },
            failure: function (data) {
                console.log(data);
            }
        });

    })
});

function logout() {
    $.ajax({
        url: '/site/logout',
        type: 'get',
        success: function (data) {
            location.href = '/';
        },
        failure: function (data) {
            console.log(data);
        }
    });
}

function validateForm() {
    let forms = document.querySelectorAll('.needs-validation')
    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
        })
}
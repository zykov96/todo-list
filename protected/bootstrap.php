<?php

use Illuminate\Database\Capsule\Manager;

include __DIR__ . '/../vendor/autoload.php';
include __DIR__ . '/core/App.php';

__includeFiles();

$capsule = new Manager();
$capsule->addConnection([
    'driver' => 'pgsql',
    'host' => '192.20.2.216',
    'database' => 'todo_list',
    'username' => 'admin',
    'password' => '61KUOu%vgO2W',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();

new app\core\App([]);

function __includeFiles($dir = __DIR__)
{
    foreach (scandir($dir) as $file) {
        if ($file == '.' || $file == '..' || $file == 'bootstrap.php'
            || $file == 'bootstrap' || $file == 'views' || $file == 'core' || $file == 'migrates')
            continue;
        if (is_dir($nextDir = __DIR__ . DIRECTORY_SEPARATOR . $file)) {
            __includeFiles($nextDir);
        } else {
            require $dir . DIRECTORY_SEPARATOR . $file;
        }
    }
}
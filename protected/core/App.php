<?php
namespace app\core;
class App
{
    public function __construct($config)
    {
        $controller_name = 'Site';
        $action_name = 'Index';

        $dirty_uri = explode("?", $_SERVER["REQUEST_URI"]);
        $routes = explode("/", $dirty_uri[0]);

        if ($routes[1] == 'favicon.ico') return;

        if (strstr($routes[1], 'bootstrap')) {
            return;
        }

        if (!empty($routes[1])) {
            $controller_name = ucfirst($routes[1]);
        }

        if (!empty($routes[2])) {
            $action_name = ucfirst($routes[2]);
        }

        $controller = "app\\controllers\\{$controller_name}Controller";

        $controller = new $controller();

        if (method_exists($controller, 'action'.$action_name)) {
            $controller->{'action'.$action_name}();
        } else {
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
            header('HTTP/1.1 404 Not Found');
            header("Status: 404 Not Found");
            header('Location:'.$host.'404');
        }

    }
}
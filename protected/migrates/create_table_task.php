<?php

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Schema\Blueprint;

include __DIR__ .'/../../vendor/autoload.php';

$capsule = new Manager();
$capsule->addConnection([
    'driver' => 'pgsql',
    'host' => '192.20.2.216',
    'database' => 'todo_list',
    'username' => 'admin',
    'password' => '61KUOu%vgO2W',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();

Manager::schema()->create('task', function (Blueprint $table) {
    $table->increments('id')->autoIncrement();
    $table->string('user_name');
    $table->string('email');
    $table->string('body');
    $table->boolean('is_complete');
});

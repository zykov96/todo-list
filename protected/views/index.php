<?php
/** @var array $tasks */
/** @var int $pages */

?>
    <div class="row" style="padding-top: 10px">
        <div class="col"></div>
        <div class="col"></div>
        <div class="col"></div>
        <div class="col"></div>
        <div class="col"></div>
        <div class="col"></div>
        <div class="col"></div>
        <div class="col">
            <a class="btn btn-primary" href="/site/create">Создать задачу</a>
        </div>
    </div>
    <div class="row" style="padding: 10px 10px">
        <table class="table table-bordered"
               data-ajax="ajaxRequest">
            <thead class="thead-dark">
            <tr>
                <th scope="col" id="id">#</th>
                <th scope="col" id="user_name">Имя пользователя</th>
                <th scope="col" id="email">Email</th>
                <th scope="col" id="is_complete">Выполнена?</th>
                <?php if (!empty($_SESSION)) echo '<th scope="col">Действия</th>'; ?>
            </tr>
            </thead>
            <tbody>
            <?php
            if (empty($tasks)) { ?>
                <tr>
                    <td colspan="4" align="center"> Данные отсутствуют</td>
                </tr>
                <?php
            } else {
                foreach ($tasks as $task) { ?>
                    <tr>
                        <td><?= $task->id ?></td>
                        <td><?= $task->user_name ?></td>
                        <td><?= $task->email ?></td>
                        <td><?= $task->is_complete ? 'Да' : 'Нет' ?></td>
                        <?php if (!empty($_SESSION)) echo "<td scope='col'><a class='btn btn-secondary' href='/site/update?id={$task->id}'>Редактировать</a></td>"; ?>

                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>
    </div>
<?php if (!empty($tasks)) { ?>
    <div class="row">
        <nav class="pagination-container">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link disabled" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <?php
                for ($i = 0; $i < $pages; $i++) {
                    $num = $i + 1;
                    if ($i == 0)
                        echo "<li class=\"page-item\"><a class=\"page-link active\" href=\"?page=$num\">$num</a></li>";
                    else
                        echo "<li class=\"page-item\"><a class=\"page-link\" href=\"?page=$num\">$num</a></li>";
                }
                ?>
                <li class="page-item">
                    <a class="page-link disabled" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
<?php } ?>
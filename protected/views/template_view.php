<?php /** @var string $content_view */ ?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/web/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="/web/css/main.css">
    <script src="/web/bootstrap/js/bootstrap.js"></script>
    <script src="https://code.jquery.com/jquery-3.7.0.min.js"></script>
    <script src="/web/js/main.js"></script>
    <script src="/web/js/paginathing.min.js"></script>
    <title>Список задач</title>
</head>
<body class="offcanvas-body">
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="/">Список задач</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <?php
                if (!empty($_SESSION)) {
                    echo '<li class="nav-item active">
                        <a class="nav-link" href="/site/logout" onclick="logout()">Выйти <span class="sr-only"></span></a>
                    </li>';
                } else {
                    echo '<li class="nav-item active">
                        <a class="nav-link" href="/site/login">Войти <span class="sr-only"></span></a>
                    </li>';
                }
                ?>

            </ul>
        </div>
    </nav>

    <?php include 'protected/views/' . $content_view; ?>

</div>
</body>
</html>

<div class="row">
    <form class="row needs-validation" name="task" id="task" action="/site/create">
        <div class="col-md-6">
            <label for="user_name" class="form-label">Имя пользователя</label>
            <input class="form-control" id="user_name" name="user_name" type="text" required>
            <div class="invalid-feedback">Необходимо заполнить поле</div>
        </div>
        <div class="col-md-6">
            <label for="email" class="form-label">Электронная почта</label>
            <input class="form-control" type="email" id="email" name="email" aria-describedby="email" required>
            <div class="invalid-feedback">Необходимо заполнить поле</div>
        </div>
        <div class="col-md-8">
            <label for="body" class="form-label">Описание задачи</label>
            <textarea class="form-control" name="body" id="body"></textarea>
            <div class="invalid-feedback">Необходимо заполнить поле</div>
        </div>
        <div class="col-12">
            <input class="btn btn-primary" type="submit" value="Создать" id="submit">
        </div>
    </form>

</div>
<div class="row">
    <?= $alert ?? '' ?>
</div>
<?php

<?php if (!($is_success ?? false)) { ?>

    <div class="row">
        <form action="/site/login" method="post" name="login" id="login">
            <div class="col-md-8">
                <label for="user_name" class="form-label">Имя пользователя</label>
                <input type="text" class="form-control" name="user_name" id="user_name">
                <div class="invalid-feedback">Необходимо заполнить поле</div>
            </div>
            <div class="col-md-8">
                <label for="password" class="form-label">Пароль</label>
                <input type="password" class="form-control" name="password" id="password">
                <div class="invalid-feedback">Необходимо заполнить поле</div>
            </div>

            <div class="col-12">
                <input class="btn btn-primary" type="submit" value="Войти">
            </div>
        </form>
    </div>
<?php } ?>
    <div class="row">
        <?php echo $alert ?? '' ?>
    </div>
<?php

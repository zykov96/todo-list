<?php
/** @var Task $model */

use app\models\Task;
?>
<div class="row">
    <form class="row needs-validation" id="task" action="/site/update?id=<?=$model->id ?>">
        <input type="hidden" name="id" value="<?=$model->id ?>">
        <div class="col-md-6">
            <label for="user_name" class="form-label">Имя пользователя</label>
            <input class="form-control" id="user_name" name="user_name" value="<?=$model->user_name?>" type="text" required>
            <div class="invalid-feedback">Необходимо заполнить поле</div>
        </div>
        <div class="col-md-6">
            <label for="email" class="form-label">Электронная почта</label>
            <input class="form-control" type="email" id="email" name="email" value="<?=$model->email?>" aria-describedby="email" required>
            <div class="invalid-feedback">Необходимо заполнить поле</div>
        </div>
        <div class="col-md-6">
            <label for="is_complete" class="form-label">Выполнена?</label>
            <input class="form-check-input" type="checkbox" id="is_complete" name="is_complete" aria-describedby="is_complete" <?=$model->is_complete ? 'checked' : ''?>>
            <div class="invalid-feedback">Необходимо заполнить поле</div>
        </div>
        <div class="col-md-8">
            <label for="body" class="form-label">Описание задачи</label>
            <textarea class="form-control" name="body" id="body"><?=$model->body?></textarea>
            <div class="invalid-feedback">Необходимо заполнить поле</div>
        </div>
        <div class="col-12">
            <input class="btn btn-primary" type="submit" value="Обновить" id="submit">
        </div>
    </form>
</div>

<div class="row">
    <?= $alert ?? '' ?>
</div>


<?php

namespace app\models;

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Model;

/**
 * @property $id
 * @property $user_name
 * @property $email
 * @property $body
 * @property $is_complete
 */
class Task extends Model
{
    public $timestamps = false;

    protected $table = 'task';
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $casts = [
        'user_name' => 'string',
        'email' => 'string',
        'body' => 'string',
        'is_complete' => 'bool',
    ];

    protected $guarded = [];
    protected $fillable = [
        'id',
        'user_name',
        'email',
        'body',
        'is_complete',
    ];
}
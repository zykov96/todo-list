<?php

namespace app\controllers;

use app\models\Task;
use Illuminate\Database\Capsule\Manager;

class SiteController
{
    public function actionIndex()
    {

        $this->checkSession();

        $per_page = 3;
        $content_view = 'index.php';

        if (array_key_exists('page', $_GET)) {
            $page = $_GET['page'];
        }

        $all_task = Task::all()->count();

        $pages = round($all_task / $per_page);

        $tasks = Task::all()->forPage($page ?? 1, $per_page);

        include "protected/views/template_view.php";
    }

    public function actionCreate()
    {

        $content_view = 'create.php';
        if (!empty($_POST)) {

            $model = new Task([
                'user_name' => $_POST['user_name'],
                'email' => $_POST['email'],
                'body' => $_POST['body'],
                'is_complete' => false,
            ]);

            if ($model->save()) {
                $alert = '<div class="alert alert-success">Задача успешно создана</div>';
            } else {
                $alert = '<div class="alert alert-danger">Ошибка при сохранении</div>';
            }
        }

        include "protected/views/template_view.php";
    }

    public function actionUpdate()
    {
        $this->checkSession();

        if (!empty($_POST)) {
            $model = Task::query()->find($_GET['id']);
            if ($model->update([
                'user_name' => $_POST['user_name'],
                'email' => $_POST['email'],
                'body' => $_POST['body'],
                'is_complete' => isset($_POST['is_complete']),
            ])) {
                $alert = '<div class="alert alert-success">Задача успешно обновлена</div>';
            } else {
                $alert = "<div class=\"alert alert-danger\">Ошибка при обновлении записи</div>";
            }

            $model->refresh();
            $content_view = 'update.php';
        } else if (!empty($_GET) && array_key_exists('id', $_GET) && $_GET['id']) {
            $model = Task::query()->find($_GET['id']);
            $content_view = 'update.php';
        } else {
            $content_view = 'index.php';

        }

        include "protected/views/template_view.php";
    }

    public function actionLogin()
    {
        $content_view = 'login.php';

        if (!empty($_POST)) {
            if ($_POST['user_name'] == 'admin' && $_POST['password'] == '123') {
                session_start([
                    'cookie_lifetime' => 86400,
                ]);
                $_SESSION['id'] = session_id();

                $alert = "<div class=\"alert alert-success\">Успешная авторизация</div>";
                $is_success = true;
            } else {
                $alert = "<div class=\"alert alert-danger\">Неверный логин или пароль</div>";

            }
        }

        include "protected/views/template_view.php";

    }

    public function actionLogout()
    {
        $this->checkSession();
        session_destroy();

        $content_view = 'index.php';
        include "protected/views/template_view.php";

    }


    private function checkSession()
    {
        if (session_id() === '') {
            session_start();
        }
    }
}